package Modello;

import java.util.List;

import Entity.Brick;

public class SurvivalGameModel extends AbstractGameModel {

    @Override
    protected List<Brick> doIfBricksRowIsEmpty(Brick template) {
        List<Brick> row = factory.randomBrickRow();
        for(Brick b: row) {
            b.setY(template.getY());
        }
        return row;
    }

}
