package Modello;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import Entity.*;
import View.ImageViewObject;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;

public class EntityFactory implements IEntityFactory{
    private static final int WINDOW_WIDTH = 720;
    private static final int WINDOW_HEIGHT = 720;
    
    //li metterei nel model e li passerei al costruttore
    private static final int BRICKS_ROW = 3;
    private static final int BRICKS_COLUMN = 15 ;
    
    private static final int MAX_DELTA_ANGLE = 25;
    private static final int MIN_DELTA_ANGLE = 5;
    
    public Ball standardBall() {
        //il -1 serve a non prendere l'ultima tipologia di pallina -> quella di fuoco
        BallType type = BallType.values()[new Random().nextInt(BallType.values().length-1)];
        return new Ball(WINDOW_WIDTH/2, WINDOW_HEIGHT/2, type);
    }
    
    public List<Ball> multipleBall(Ball b) {
        List<Ball> balls = new ArrayList<>();
        int angle = new Random().nextInt(MAX_DELTA_ANGLE)+MIN_DELTA_ANGLE;
        balls.add(new Ball(b, angle));
        balls.add(new Ball(b, -angle));
        return balls;
    }
    
    public List<Wall> standardWalls(){
        List<Wall> walls = new ArrayList<>();
        walls.add(new VerticalWall(0, 0, WINDOW_HEIGHT));
        walls.add(new HorizontalWall(0, WINDOW_WIDTH, 0));
        walls.add(new VerticalWall(WINDOW_WIDTH, 0, WINDOW_HEIGHT));
        
        //parete inferiore inutile 
       // walls.add(new HorizontalWall(0, WINDOW_WIDTH, WINDOW_HEIGHT));
        return walls;
    }
    
    private Brick randomBrick() {
        double brickWidth = (WINDOW_WIDTH - BRICKS_COLUMN) / BRICKS_COLUMN;
        /*Brick rndBrick = BrickEnum.values().get(new Random().nextInt(BrickEnum.values().length)); 
        rndBrick.setWidth(brickWidth);
        rndBrick.setHeight(brickWidth / 2);*/
        Brick b = new Brick(brickWidth, brickWidth/2); //rndBrick
        
        //b.setFill(new ImagePattern(ImageViewObject.LATERAL_WALL.getImage())); poi non servir�
        return b;
    }
    
    public List<Brick> randomBrickRow() {
        List<Brick> rowBrick = new ArrayList<>();
        for(int i = 0; i < BRICKS_COLUMN; i++) {
            Brick b = randomBrick();
            b.setX(i + i * b.getWidth());
            rowBrick.add(b);
        }
        return rowBrick;
    }
    
    //optional di brick se vogliamo fare delle figure con i mattoncini?
    public Map<Integer, List<Brick>> randomBrickMap() {
        Map<Integer, List<Brick>> mapBrick = new HashMap<>();
        for(int i = 0; i < BRICKS_ROW; i++) {
            List<Brick> list = randomBrickRow();
            for(Brick b : list) {
                b.setY(i + i * b.getHeight());
            }
             mapBrick.put(i, list);
        }
        return mapBrick;
    }
     
    public Bar standardBar() {
        return new Bar();
    }
    
    
    /*public PowerUp randomPowerUp() {
        return new PowerUp();
    }*/
}
