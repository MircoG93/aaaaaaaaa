package Modello;

import java.util.List;
import java.util.Map;

import Entity.*;

public interface IEntityFactory {
    public Ball standardBall();
    
    public List<Ball> multipleBall(Ball b);
    
    public List<Wall> standardWalls();
    
    public List<Brick> randomBrickRow();
    
    //optional di brick se vogliamo fare delle figure?
    public Map<Integer, List<Brick>> randomBrickMap();
    
    public Bar standardBar();
}
