package Modello;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import Entity.*;
import controller.ObserverConcreto;
import javafx.util.Pair;

public abstract class AbstractGameModel extends ObserverConcreto{    
    private static final int LIVES_ON_START = 3;
    private static final int SCORE_ON_START = 0;

    //mettere tutto private quello che nn uso nelle specializzazioni
    private GameStatus status;
    private int score;
    private int lives;

    private  List<Ball> balls;
    protected Map<Integer,List<Brick>> bricks;
    private List<Wall> walls;
    private Bar bar;

    protected IEntityFactory factory;

    public AbstractGameModel() {
        this.status = GameStatus.START;
        this.score = SCORE_ON_START; 
        this.lives = LIVES_ON_START;

        this.factory = new EntityFactory();
        this.balls = new ArrayList<>();

        this.balls.add(factory.standardBall());
        
        this.bricks = factory.randomBrickMap();
        this.walls = factory.standardWalls();
        this.bar = factory.standardBar();
    }

    //  ** GESTIONE DEL GIOCO ** //

    //tutti quei cicli si potrebbe fare un metodo generico?
    public void updateModel() {
        for(Ball c : this.balls) {
            c.refreshPosition();

            if(c.getCenterY() > 720 - c.getRadius()){
                lostBall(c);
                break;
            }

            for(Wall l : this.walls) {
                c.checkCollision(l);
            }
            
            List<Pair<Integer,Brick>> brickToRemove = new ArrayList<>();
            for(Entry<Integer, List<Brick>> entry : this.bricks.entrySet()) {
                for(Brick b : entry.getValue()) {
                    if(c.checkCollision(b)) {
                        brickToRemove.add(new Pair<>(entry.getKey(), b));
                    }
                }
                
            }
            
            for(Pair<Integer,Brick> pair:brickToRemove) {
                breakBrick(pair.getKey(), pair.getValue());
            }

            c.checkCollision(this.bar);


            /* notifica observer (posizione pallina)?*/
            //this.notifyIObservers(c); //non serve xke si aggiorna in automatico!
        }
    }
    private void lostBall(Ball b) {
        this.balls.remove(b);
        notifyRemove(b);
        if(this.balls.size() == 0) {
            this.status = GameStatus.START;
            lostLive();
        }
    }

    private void lostLive() {
        this.lives--;
        notifyUpdate(this.score, this.lives);
        if(this.lives == 0) {
            this.status = GameStatus.LOST;
            //  notifyObservers(this.status);
        }
    }

    //multipallina // ci va anche con la factory?
    public void addBall(Ball b) { //???public?
        this.balls.add(b);
        notifyAdd(b);
    }
    /*
    public void addBalls(List<Ball> b) { //puo servire x aggiungere le multiple
        for(Ball ball:b) {
            addBall(ball);
        }
    }*/

    private void breakBrick(Integer key, Brick b) {
        /*List<Brick> newRow = this.bricks.get(key); //un array della stessa dimensione senn� non funziona la copy
        Collections.copy(newRow, this.bricks.get(key));*/
        List<Brick> newRow = this.bricks.get(key); //
        newRow.remove(b);
        notifyRemove(b);

        if(newRow.isEmpty()) {
            newRow = doIfBricksRowIsEmpty(b);
        }
        
        this.bricks.replace(key, newRow);
        
        for(Brick brick : newRow) {
            notifyAdd(brick);
        }

        incScore(b);
        if(counterBrick() == 0) {
            this.status = GameStatus.WON;
            //   notifyObservers(this.status);
        }
    }

    protected abstract List<Brick> doIfBricksRowIsEmpty(Brick template);

    private void incScore(Brick b) {
        //this.score += b.getScoreOnBreak();
        this.score++;
        notifyUpdate(this.score, this.lives);
    }

    //nn so se ci va credo di si anche se altri lo han messo nel controller
    public void setPause() {
        if(this.status.equals(GameStatus.RUNNING)) {
            this.status = GameStatus.PAUSE;
        } else if(this.status.equals(GameStatus.PAUSE) || this.status.equals(GameStatus.START)) {
            this.status = GameStatus.RUNNING;
        }
    }

    //  **  GETTER  **  //
    //  Entity getter
    public List<Ball> getBalls() {
        return balls;
    }

    public Map<Integer,List<Brick>> getBricks() {
        return bricks;
    }

    public List<Wall> getWalls() {
        return walls;
    }

    public Bar getBar() {
        return bar;
    }

    // altri getter
    public GameStatus getStatus() {
        return status;
    }

    public int getScore() {
        return score;
    }

    public int getLives() {
        return lives;
    }

    //  ** UTILITA'  **  //
    private int counterBrick() {
        int count = 0;
        for(List<Brick> l : this.bricks.values()) {
            count += l.size();
        }
        return count;
    }

    @Override
    public String toString() {
        return "GameModel [status=" + status + ", score=" + score + ", lives=" + lives + "]";
    }
}
