package Modello;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import Entity.Brick;

public class ClassicGameModel extends AbstractGameModel {

    public ClassicGameModel() {
        super();        
    }

    @Override
    protected List<Brick> doIfBricksRowIsEmpty(Brick template) {
        return new ArrayList<>();
    }
}
