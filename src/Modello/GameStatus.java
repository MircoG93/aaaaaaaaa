package Modello;

public enum GameStatus {
    START,
    WON,
    LOST,
    RUNNING,
    PAUSE;
}
