/**
 * 
 */
package Modello;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import javafx.util.Pair;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * @author Mirco Gnoli
 *
 * A singleton class
 */
public final class HighscoreManager {
    private static HighscoreManager instance;

    private static final String PATH = System.getProperty("user.home") + System.getProperty("file.separator");
    private static final String MODE_CLASSIC = "Classic";
    private static final String MODE_SURVIVAL = "Survival";

    private List<Pair<String, Integer>> classicScore;
    private List<Pair<String, Integer>> survivalScore;

    private HighscoreManager(){
        this.classicScore = loadScore(MODE_CLASSIC);
        this.survivalScore = loadScore(MODE_SURVIVAL);  
    }

    public static HighscoreManager getInstance(){
        if (Objects.isNull(instance)) {
            instance = new HighscoreManager();
        }
        return instance;
    }

    public void addScore(String name, Integer score, GameMode mode) {
        Pair<String, Integer> s = new Pair<>(name, score);
        switch(mode){
        case CLASSIC:
            this.classicScore.add(s);
            Collections.sort(this.classicScore, new MyCompare());
            
        case SURVIVAL:
            this.survivalScore.add(s);
            Collections.sort(this.survivalScore, new MyCompare());
        }
    }

    
    //@SuppressWarnings("unchecked")
    private List<Pair<String, Integer>> loadScore(String mode){
        List<Pair<String, Integer>> load = new ArrayList<>();
        File f = new File(PATH+mode);
        try {
            f.createNewFile();

            if(f.length()>0) {
                final InputStream file = new FileInputStream (PATH+mode);
                final InputStream bstream = new BufferedInputStream (file);
                ObjectInputStream ostream = new ObjectInputStream (bstream);

                load = (List<Pair<String, Integer>>) ostream.readObject();
                ostream.close();
            }
            
        } catch (IOException e) {
            System.out.println("Errore file " + f + " corrotto. Eliminare il file e riavviare l'applicazione");
            e.printStackTrace();
        }catch (ClassNotFoundException e) {
            //Non dovrebbe mai entrare qui
            e.printStackTrace();
        }
        return load;
    }

    public void saveAllScores(){
        saveScore(MODE_CLASSIC, classicScore);
        saveScore(MODE_SURVIVAL, survivalScore);
    }
    
    private void saveScore(String mode, List<Pair<String, Integer>> list){
        try {
            final OutputStream file = new FileOutputStream (PATH + mode);
            final OutputStream bstream = new BufferedOutputStream (file);
            final ObjectOutputStream ostream = new ObjectOutputStream (bstream);

            ostream.writeObject(list);
            ostream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Pair<String, Integer>> getScore(GameMode mode){
        switch(mode){
        case CLASSIC:
            return this.classicScore;
            
        case SURVIVAL:
            return this.survivalScore;
            
            default:
                return null;
        }
    }
    
    public class MyCompare implements Comparator<Pair<String, Integer>>{

        @Override
        public int compare(Pair<String, Integer> o1, Pair<String, Integer> o2) {
            return Integer.compare(o1.getValue(), o2.getValue());
        }
        
    }
}
