package controller;
import java.util.List;

import Entity.IEntity;
import Modello.AbstractGameModel;
import Modello.ClassicGameModel;
import Modello.GameStatus;
import Modello.SurvivalGameModel;
import View.ArenaView;
import View.GameView;
import javafx.scene.Node;
import javafx.stage.Stage;

public class Controller implements Runnable {
    AbstractGameModel m;
    GameView v;
    
    public Controller(Stage primaryStage){
        //this.m = new ClassicGameModel();
        this.m = new SurvivalGameModel();
        this.v = new GameView(this, primaryStage);
        
        m.addIObserver(new IObserver() {
            
            @Override
            public void updateValue(int score, int lives) {
                v.getInfoPanel().refresh(score, lives);
            }
            
            @Override
            public void removeEntity(IEntity ent) {
                v.getArena().removeEntity(ent);
            }
            
            @Override
            public void addEntity(IEntity ent) {
                v.getArena().addEntity(ent);
            }
        });
    }
    
    public List<? extends Node> getEntity() {
        return m.getBalls();
    }
    
    public AbstractGameModel getModel() {
        return this.m;
    }
    
    @Override
    public void run() {
        int i = 0;
        while (this.m.getStatus() != GameStatus.PAUSE && this.m.getStatus() != GameStatus.WON ) { //non ha perso
            i++;
            this.m.updateModel();             //aggiorna il model
            
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
    }
    
    
}
