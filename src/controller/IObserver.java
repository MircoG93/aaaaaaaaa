package controller;

import Entity.*;

public interface IObserver {
    public void addEntity (IEntity ent);
    public void removeEntity (IEntity ent);
    public void updateValue (int score, int lives);
    
    //public void updateStatus (GameStatus status);
}
