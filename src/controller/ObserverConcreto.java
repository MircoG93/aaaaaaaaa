package controller;

import java.util.HashSet;
import java.util.Set;

import Entity.IEntity;

public class ObserverConcreto {
    private final Set <IObserver> set = new HashSet <>();

    public void addIObserver (IObserver obs){
        this.set.add(obs);
    }

    public void notifyAdd (IEntity arg){
        for ( final IObserver obs : this.set){
            obs.addEntity(arg);
        }
    }
    
    public void notifyRemove (IEntity arg){
        for ( final IObserver obs : this.set){
            obs.removeEntity(arg);
        }
    }
    
    public void notifyUpdate(int score, int lives){
        for ( final IObserver obs : this.set){
            obs.updateValue(score, lives);
        }
    }
}
