
import controller.Controller;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application{

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
//        primaryStage.setWidth(720);
//        primaryStage.setHeight(720); //- 30 x la window. Misura finale del campo 720 * 690
//        primaryStage.setResizable(false); //volendo si pu� anche togliere questa opzione ed adattare mattoni e barra alla dim schermo ogni ciclo
        /* 
        Code for JavaFX application. 
        (Stage, scene, scene graph) 
        */
        Thread t = new Thread(new Controller(primaryStage));
        t.start();
    }

}