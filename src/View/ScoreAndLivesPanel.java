package View;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

public class ScoreAndLivesPanel extends HBox {
    private final Font font = new Font("Comic Sans MS", 15);
    
    Label scoreL;
    Label livesL;
    
    public ScoreAndLivesPanel() {
        super();
        this.setPadding(new Insets(15, 15, 15, 15)); //top right bottom left
        this.setSpacing(25); //secondo me non ci va, splitto il pannello in 2
        
        scoreL = new Label("Score: 0");
        livesL = new Label("Lives: 3");
        this.getChildren().addAll(scoreL, livesL);
        
        for(Node c : this.getChildren()) {
            if(c instanceof Label) {
                ((Label)c).setFont(font);
            }
        }
        
    }
    
    public void refresh(int score, int lives) {
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                scoreL.setText("Score: " + score);
                livesL.setText("Lives: " + lives);
            }
         });
    }
    
}
