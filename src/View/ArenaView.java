package View;

import java.util.List;

import Entity.*;
import controller.Controller;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;

public class ArenaView extends Pane{
    Controller c;
    
    public ArenaView(Controller c){
        this.c = c;
        
        this.setPrefWidth(720); //settarli come costanti
        this.setPrefHeight(720);
        BackgroundImage bgimg = new BackgroundImage(ImageViewObject.IMGVARIA.getImage(),  BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        this.setBackground(new Background(bgimg));
        
        this.getChildren().addAll(c.getModel().getBalls());
        this.getChildren().addAll(c.getModel().getWalls());
        this.getChildren().add(c.getModel().getBar());
        
        for(List<Brick> list : c.getModel().getBricks().values()) {
            this.getChildren().addAll(list);
        }
    }

    public void removeEntity(IEntity ent) {
        Platform.runLater(new Runnable(){

            @Override
            public void run() {
                getChildren().remove((Node) ent);
            }
         });
    }
    
    public void addEntity(IEntity ent) {
        Platform.runLater(new Runnable(){

            @Override
            public void run() {
                if(!getChildren().contains((Node)ent)) {
                    getChildren().add((Node) ent);
                }
            }
         });
    }
}
