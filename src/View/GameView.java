package View;

import controller.Controller;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class GameView {

    BorderPane layout;
    public GameView(Controller c, Stage primaryStage) {
        
        primaryStage.setTitle("PrimaryStage");
        
        Pane bottom = new ScoreAndLivesPanel();
        Pane central = new ArenaView(c);
        
        layout = new BorderPane();
        layout.setCenter(central);
        layout.setBottom(bottom);
        
        
        layout.setRight(lateralBorder());
        layout.setLeft(lateralBorder());
        layout.setTop(horizontalBorder());
        
        Scene scene = new Scene(layout);
        
        primaryStage.setScene(scene);
        
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    public ArenaView getArena() {
        return (ArenaView) layout.getCenter();
    }
    
    public ScoreAndLivesPanel getInfoPanel() {
        return (ScoreAndLivesPanel) layout.getBottom();
    }
    
    private Pane lateralBorder() {
        Pane p = new Pane();
        BackgroundImage bgimg = new BackgroundImage(ImageViewObject.LATERAL_WALL.getImage(),  BackgroundRepeat.NO_REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        p.setBackground(new Background(bgimg));
        p.setPrefWidth(ImageViewObject.LATERAL_WALL.getImage().getWidth());
        return p;
    }
    
    private Pane horizontalBorder() {
        Pane p = new Pane();
        BackgroundImage bgimg = new BackgroundImage(ImageViewObject.HORIZONTAL_WALL.getImage(),  BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        p.setBackground(new Background(bgimg));
        p.setPrefHeight(ImageViewObject.HORIZONTAL_WALL.getImage().getHeight());
        return p;
    }
}



//layout.setBorder(new Borde);
//layout.setBorder(new Border(new BorderImage(ImageViewObject.LATERAL_WALL.getImage(), new BorderWidths(0), Insets.EMPTY, new BorderWidths(0), true, BorderRepeat., BorderRepeat.ROUND)));
//layoutMaggiore.setBorder(new Border(new BorderStroke(Color.DARKSLATEBLUE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(10))));
/*cos� mi crea la colonna laterale!
BackgroundImage bgimg = new BackgroundImage(ImageViewObject.LATERAL_WALL.getImage(),  BackgroundRepeat.NO_REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
layout.setBackground(new Background(bgimg));*/