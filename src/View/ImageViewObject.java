package View;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.scene.image.Image;

public enum ImageViewObject {
    //siccome le pareti sono linee, non hanno riempimento
    LATERAL_WALL("Wall_Lat.png"),
    HORIZONTAL_WALL("Top_Wall.png"),
    IMGVARIA("sfondo.jpg");

    private final String PATH = ""; //System.getProperty("user.home") + System.getProperty("file.separator");
    Image image;
    
    private  ImageViewObject(String name) {
        try {
            this.image = new Image(new FileInputStream(PATH + name));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Image getImage() {
        return this.image;
    }
    
}
