package Entity;

public interface IMovingEntity extends IEntity {
    void refreshPosition();   
}
