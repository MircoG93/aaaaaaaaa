package Entity;

import javafx.scene.paint.*;
public enum BallType {
    BLUE_BALL(Color.BLUE),
    BLACK_BALL(Color.BLACK),
    GREEN_BALL(Color.GREEN),
    FIRE_BALL(Color.RED);
    
    private Paint paint;
    
    private BallType(Paint p) {
        this.paint = p;
    }
    
    public Paint getPaint() {
        return this.paint;
    }    
}
