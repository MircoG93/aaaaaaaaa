package Entity;

public enum PositionOfCollision {
    UP,
    DOWN,
    RIGHT,
    LEFT;
}
