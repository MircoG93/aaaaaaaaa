package Entity;

import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

public class Brick extends Rectangle implements IEntity {

    public Brick(double x, double y, double width, double height) {
        super(x, y, width, height);
    }
    
    public Brick(double width, double height) {
        super(width, height);
    }


}
