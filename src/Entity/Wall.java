package Entity;

import javafx.scene.shape.Line;

public abstract class Wall extends Line implements IEntity {
    
    public Wall(double startX, double startY, double endX, double endY) {
        super(startX, startY, endX, endY);
        this.setStrokeWidth(1);
    }
    boolean isHorizontalWall() {
        return this instanceof HorizontalWall;
    }
    
    boolean isVerticalWall() {
        return this instanceof VerticalWall;
    }
    
}
