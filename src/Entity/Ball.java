package Entity;

import javafx.geometry.Bounds;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;

public class Ball extends Circle implements IMovingEntity {
    
    private static final int DEFAULT_SPEED = 10;
    private static final int DEFAULT_RADIOUS = 5;
    private static final int DEFAULT_ANGLE = 150;
    /*
    private static final int CHANGE_ANGLE = 10;
    private int countCollision;
    */
    private int speed;
    private double angle;
    private BallType type;

    /** COSTRUTTORI **/
    public Ball(double x, double y, BallType type) {
        this(x, y, DEFAULT_SPEED, DEFAULT_ANGLE, type);
    }

    public Ball(double x, double y, int speed, double angle, BallType type) {
        super(x, y, DEFAULT_RADIOUS, type.getPaint());
        this.speed = speed;
        this.angle = angle % 360;
        this.type = type;
    }
    
    public Ball(Ball b, int deltaAngle) {
        this(b.getCenterX(), b.getCenterY(), b.getSpeed(), b.getAngle() + deltaAngle, b.getType());
    }


    /** COMPORTAMENTO **/
    @Override
    public void refreshPosition() {
        double newX = this.getCenterX() + this.speed * Math.cos(Math.toRadians(angle));
        double newY = this.getCenterY() + this.speed * Math.sin(Math.toRadians(angle));
        this.setPosition(newX, newY);
        
    }
    
    //? extends shape
    public boolean checkCollision(Shape s) {
        Bounds sBound = s.getBoundsInParent();
        
        if(this.intersects(sBound)) {
            //se la shape � una barra -> rimbalza strano
            //se la shape � la parete -> rimbalza standard
            //se la shape � un mattone che non si rompe oppure la palla � normale -> rimbalza standard
                    //se la shape � un mattone che si rompe e la palla e di fuoco -> continua ad andare 
            
            if(this.type != BallType.FIRE_BALL || s instanceof Wall || (s instanceof Brick) ) { //aggiungere il controllo && mattone indistruttibile
                standardCollision(sBound);
            }/*
            
            if(s instanceof Bar) {
                barCollision();
            }
            
            if(s instanceof Brick che si rompe && this.tipo==fuoco) {
                fai una cippa alla pallina
                sto controllo secondo me neanche ci va
            }*/
            return true;
        }
        return false;
    }
    
    private void standardCollision(Bounds bound) {
        if(dirRight()) {
            if(collidedWithLeftestVerticalBound(bound)) {
                this.setPosition(bound.getMinX() - getRadius(), this.getCenterY());
                this.bounce(PositionOfCollision.RIGHT);
            }
        }

        if(dirLeft()) {
            if(collidedWithRightestVerticalBound(bound)) {
                this.setPosition(bound.getMaxX() + getRadius(), this.getCenterY());
                this.bounce(PositionOfCollision.LEFT);
            }
        }

        if(dirUp()) {
            if(collidedWithLowerHorizontalBound(bound)){
                this.setPosition(this.getCenterX(), bound.getMaxY() + getRadius());
                this.bounce(PositionOfCollision.UP);
            }
        }

        if(dirDown()) {
            if(collidedWithTopHorizontalBound(bound)){
                this.setPosition(this.getCenterX(), bound.getMinY() - getRadius());
                this.bounce(PositionOfCollision.DOWN);
            }
        }
    }

    private void barCollision(Bounds bound) {
        //se cade nel primo terzo rimbalza a sx
      
        //if cade nel mezzo:
        this.setPosition(this.getCenterX(), bound.getMaxY() - getRadius());
        this.bounce(PositionOfCollision.DOWN);
        
        //se cade nell'ultimo pezzo, rimbalza a dx
        
    }
    
    public void bounce(PositionOfCollision pos) {

        switch (pos) {
        case UP:
            if (this.angle > 270) {
                this.angle = 360 - this.angle;
            } else {
                this.angle = 180 - (this.angle - 180);
            }
            break;
        case DOWN:
            if(this.angle < 90) {
                this.angle = 360 - this.angle;
            } else {
                this.angle = 180 + (180 - this.angle);
            }

            break;
        case RIGHT:
            if(this.angle < 90) {
                this.angle = 180 - this.angle;
            } else {
                this.angle = (360 - this.angle) + 180;
            }
            break;
        case LEFT:
            if(this.angle < 180) {
                this.angle = 180 - this.angle;
            } else {
                this.angle = 360 - (this.angle - 180);
            }
            break;
        }
    }

    public void setPosition(double newX, double newY) {
        this.setCenterX(newX);
        this.setCenterY(newY);
    }
    
    //dir sono diverse da !dirOpposta a causa degli angoli particolari 90 180 270 360/0
    private boolean dirRight() {
        return this.angle > 270 || this.angle < 90;
    }

    private boolean dirLeft() {
        return this.angle > 90 && this.angle < 270;
    }

    private boolean dirDown() {
        return this.angle > 0 && this.angle < 180;
    }

    private boolean dirUp() {
        return this.angle > 180 && this.angle < 360;
    }

    public int getSpeed() {
        return speed;
    }
    
    public double getAngle() {
        return angle;
    }
    
    public BallType getType() {
        return this.type;
    }
    
    boolean collidedWithLowerHorizontalBound(Bounds b) {
        return this.getCenterX() + getRadius() >= b.getMinX() && this.getCenterX() - getRadius() <= b.getMaxX() && this.getCenterY() + getRadius() >= b.getMaxY()? true:false;
    }

    boolean collidedWithTopHorizontalBound(Bounds b) {
        return this.getCenterX() + getRadius() >= b.getMinX() && this.getCenterX() - getRadius() <= b.getMaxX() && this.getCenterY() - getRadius() <= b.getMinY()? true:false;
    }

    boolean collidedWithRightestVerticalBound(Bounds b) {
        return this.getCenterY()- getRadius() >= b.getMinY() && this.getCenterY() + getRadius() <=b.getMaxY() && this.getCenterX() + getRadius() >= b.getMaxX()? true:false;
    }

    boolean collidedWithLeftestVerticalBound(Bounds b) {
        return this.getCenterY() - getRadius() >= b.getMinY() && this.getCenterY() + getRadius() <= b.getMaxY() && this.getCenterX() - getRadius() <= b.getMinX()? true:false;
    }
}
